﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CharacterObject : ScriptableObject
{
    public int HP;
    public float Speed;
    public int AttackDamage;
    public int AttackSpeed;
    public int Defense;
}
